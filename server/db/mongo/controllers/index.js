import topics from './topics';
import users from './users';
import channels from './channels';

export { topics, users, channels };

export default {
  topics,
  users,
  channels
};
