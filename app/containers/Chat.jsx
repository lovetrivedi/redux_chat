import React, { Component, PropTypes } from 'react';
import io from 'socket.io-client'
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { manualLogin, signUp, toggleLoginMode } from 'actions/users';


var socket = io.connect('http://localhost:3000')

/*
 * Note: This is kept as a container-level component,
 *  i.e. We should keep this as the container that does the data-fetching
 *  and dispatching of actions if you decide to have any sub-components.
 */

class Chat extends Component {
	constructor(props) {
		super(props);
		this.sendmsg = this.sendmsg.bind(this)
	}
	sendmsg(){
		var msg =  ReactDOM.findDOMNode(this.refs.msg).value;
		const username = this.props.user.user_data.profile.name;
		socket.emit('sendchat', username, msg);
	}
	render() {
		const username = this.props.user.user_data.profile.name;

		socket.on('connect', function() {
			socket.emit('adduser', username);
		})

		socket.on('updatechat', function (username, data) {
			console.log(username,data)
		})
		return (
			<div className="chat">
			 	<div>chat in public room</div>
			 	<div className="messages">
			 		<ul>
			 			<li>message 1</li>
			 			<li>message 1</li>
			 			<li>message 1</li>
			 			<li>message 1</li>
			 			<li>message 1</li>
			 		</ul>
			 	</div>
			 	<div className="message-control">
			 		<input type="text" className="message_input" ref="msg" />
			 		<input type="submit" value="send" onClick={this.sendmsg} />
			 	</div>
			</div>
			)
	}
 }
function mapStateToProps({user}) {
  return {
    user
  };
}


Chat.propTypes = {
  user: PropTypes.object,
};

export default connect(mapStateToProps)(Chat);
