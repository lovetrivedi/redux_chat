
import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames/bind';
import ChannelList from 'components/channels/ChannelList';
import ChannelForm from 'components/channels/ChannelForm';
import { createChannel, typing, destroyChannel, fetchChannels } from 'actions/channel';
import styles from 'css/components/vote';

const cx = classNames.bind(styles);

class Sidebar extends Component {

  //Data that needs to be called before rendering the component
  //This is used for server side rending via the fetchComponentDataBeforeRender() method
  static need = [  // eslint-disable-line
    fetchChannels
  ]

  componentWillMount(){
    fetchChannels()
  }
  render() {
    const {newChannle, channels, typing, createChannel , destroyChannel } = this.props;
    console.log(this.props)
    
    return (
      
     <aside className={cx('navigation')} role="navigation">
        <h4>Channeles</h4>
        <ChannelList onDestroy={destroyChannel} channels={channels} />
        <ChannelForm 
          channel={newChannle} 
          value={newChannle}
          onEntrySave={createChannel}
          className="test"
          onEntryChange={typing}
          placeholder="Channel Name"
        />
      </aside>
    );
  }
}

Sidebar.propTypes = {
  channels: PropTypes.array.isRequired,
  typing: PropTypes.func.isRequired,
  createChannel : PropTypes.func.isRequired,
  destroyChannel: PropTypes.func.isRequired,
  newChannle: PropTypes.string
};

function mapStateToProps(state) {
  return {
    channels: state.channel.channels,
    newChannle: state.channel.newChannle
  };
}
export default connect(mapStateToProps, { createChannel , typing, destroyChannel })(Sidebar);
