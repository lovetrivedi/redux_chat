import React, { Component, PropTypes } from 'react';
import classNames from 'classnames/bind';
import styles from 'css/components/topic-item';
import ChannelItem from 'components/channels/ChannelItem'
const cx = classNames.bind(styles);

export default class ChannelList extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {channels, onDestroy } = this.props;
    const ChannelItems = channels.map((channel, key) => {
    return (
      <ChannelItem 
        key={key}
        onDestroy={onDestroy}
        name={channel.name}
        id={channel.id}
      />

        );
      });

    return (
      <div>
        <ul>
          {ChannelItems}
        </ul>
      </div>
    );
  }
}

ChannelList.propTypes = {
  onDestroy: PropTypes.func.isRequired,
  channels: PropTypes.array.isRequired
};
