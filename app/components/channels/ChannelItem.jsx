import React, { Component, PropTypes } from 'react';
import classNames from 'classnames/bind';
import styles from 'css/components/topic-item';

const cx = classNames.bind(styles);

export default class ChannelItem extends Component {
  constructor(props) {
    super(props);
    this.onDestroyClick = this.onDestroyClick.bind(this);
  }
  onDestroyClick() {
    const { id, onDestroy } = this.props;
    onDestroy(id, index);
  }

  render() {
    return (
      <li>{this.props.name}</li>
    );
  } 
}

ChannelItem.propTypes = {
  name: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  onDestroy: PropTypes.func.isRequired
};
