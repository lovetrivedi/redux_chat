/* eslint consistent-return: 0, no-else-return: 0*/
import { polyfill } from 'es6-promise';
import request from 'axios';
import md5 from 'spark-md5';
import * as types from 'types';

polyfill();

/*
 * Utility function to make AJAX requests using isomorphic fetch.
 * You can also use jquery's $.ajax({}) if you do not want to use the
 * /fetch API.
 * Note: this function relies on an external variable `API_ENDPOINT`
 *        and isn't a pure function
 * @param Object Data you wish to pass to the server
 * @param String HTTP method, e.g. post, get, put, delete
 * @param String endpoint
 * @return Promise
 */



export function makeChannelRequest(method, id, data, api = '/channel') {
  return request[method](api + (id ? ('/' + id) : ''), data);
}
export function destroy(index) {
  return { type: types.DESTROY_CHANNEL, index };
}
export function typing(text) {
  return {
    type: types.TYPING,
    newChannel: text
  };
}

/*
 * @param data
 * @return a simple JS object
 */
export function createChannelRequest(data) {
  return {
    type: types.CREATE_CHANNEL_REQUEST,
    id: data.id,
    name: data.text
  };
}

export function createChannelSuccess() {
  return {
    type: types.CREATE_CHANNEL_SUCCESS
  };
}

export function createChannelFailure(data) {
  return {
    type: types.CREATE_CHANNEL_FAILURE,
    id: data.id,
    error: data.error
  };
}

export function createChannelDuplicate() {
  return {
    type: types.CREATE_CHANNEL_DUPLICATE
  };
}

// This action creator returns a function,
// which will get executed by Redux-Thunk middleware
// This function does not need to be pure, and thus allowed
// to have side effects, including executing asynchronous API calls.
export function createChannel(text) {
  return (dispatch, getState) => {
    // If the text box is empty
    if (text.trim().length <= 0) return;

    const id = md5.hash(text);
    // Redux thunk's middleware receives the store methods `dispatch`
    // and `getState` as parameters
    const { topic } = getState();
    const data = {
      count: 1,
      id,
      name:text
    };

    // Conditional dispatch
    // If the topic already exists, make sure we emit a dispatch event
    if (topic.topics.filter(topicItem => topicItem.id === id).length > 0) {
      // Currently there is no reducer that changes state for this
      // For production you would ideally have a message reducer that
      // notifies the user of a duplicate topic
      return dispatch(createChannelDuplicate());
    }

    // First dispatch an optimistic update
    dispatch(createChannelRequest(data));
    return makeChannelRequest('post', id, data)
      .then(res => {
        if (res.status === 200) {
          // We can actually dispatch a CREATE_CHANNEL_SUCCESS
          // on success, but I've opted to leave that out
          // since we already did an optimistic update
          // We could return res.json();
          return dispatch(createChannelSuccess());
        }
      })
      .catch(() => {
        return dispatch(createChannelFailure({ id, error: 'Oops! Something went wrong and we couldn\'t create your topic'}));
      });
  };
}

// Fetch posts logic
export function fetchChannels() {
  console.log('adf')
  return {
    type: types.GET_CHANNEL,
    promise: makeChannelRequest('get')
  };
}

export function destroyChannel(id, index) {
  return dispatch => {
    return makeChannelRequest('delete', id)
      .then(() => dispatch(destroy(index)))
      .catch(() => dispatch(createChannelFailure({id,
        error: 'Oops! Something went wrong and we couldn\'t add your channel'})));
  };
}
