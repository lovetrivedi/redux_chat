import {
  TYPING,
  CREATE_CHANNEL_REQUEST,
  CREATE_CHANNEL_FAILURE,
  DESTROY_CHANNEL,
  INCREMENT_COUNT,
  DECREMENT_COUNT,
  GET_CHANNEL_REQUEST,
  GET_CHANNEL_SUCCESS,
  GET_CHANNEL_FAILURE } from 'types';


export default function channel(state = {
  channels: [], 
  newChannle: ''
}, action) {
  switch (action.type) {
    case TYPING:
      return Object.assign({}, state,
        { newChannle: action.newChannel }
      );
    case GET_CHANNEL_REQUEST:
      return Object.assign({}, state, {
        isFetching: true
      });
    case GET_CHANNEL_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        channels: action.res.data
      });
    case GET_CHANNEL_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
        error: action.error
      });
    case CREATE_CHANNEL_REQUEST:
      return {
        channels: [...state.channels, { id: action.id, count: action.count, name: action.name }],
        newChannle: ''
      };
    case CREATE_CHANNEL_FAILURE:
      return {
        channels: [...state.channels.filter((tp) => tp.id !== action.id)],
        newChannle: state.newChannle
      };
    case DESTROY_CHANNEL:
      return {
        channels: [...state.channels.filter((tp, i) => i !== action.index)],
        newChannle: state.newChannle
      };
    case INCREMENT_COUNT:
      return {
        channels: [
        ...state.channels.slice(0, action.index),
        Object.assign({}, state.channels[action.index], {
          count: state.channels[action.index].count + 1
        }),
        ...state.channels.slice(action.index + 1)
        ],
        newChannle: state.newChannle
      };
    case DECREMENT_COUNT:
      return {
        channels: [
        ...state.channels.slice(0, action.index),
        Object.assign({}, state.channels[action.index], {
          count: state.channels[action.index].count - 1
        }),
        ...state.channels.slice(action.index + 1)
        ],
        newChannle: state.newChannle
      };

    default:
      return state;
  }
}
